# PicTag - A simple guide

**Parameters**

1. `PARENT_PATH` : string
    Absolute path of the root folder of the incoming pictures. The script will
    walk through all its children folders. Example: "/home/usr/Pictures"

1. `END_PATH` : string
    Absolute path of the end folder. Nested folders will be created here

1. `FORMAT` : string, list(1d, strings)
    File extension to detect pictures inside `PARENT_PATH` and inside its
    children folders. Case insensitive. If a list of formats is provided, the
    script will glob all the given format strings.
    Example: "jpg"; or ["jpg", "joppg"]

1. `PATH_FULL_FORMAT` : boolean
    Verbosity of the output paths. If True, logs show the absolute path of all
    files and folders. If False, logs show only file names or folder names.
    Note: this only changes the log format and does not have any impact on the
    file processing

1. `MAX_NESTING_DEPTH` : string
    Nesting depth to group pictures according to their creation date.
    Allowed values: ["year", "month", "day", "hour", "minute"].
    Example: "year" will create a single folder for each year, "month" will
    create year folders and month folders inside them.

1. `NESTED_FOLDERS`: dictionary
    Mapping between date fields and corresponding folder names.
    Example: "year": "anno-" creates year folders with "anno-" before the
    corresponding year e.g. "anno-2018".

### Example
```python
PARENT_PATH = "/home/mint/Pictures/"
END_PATH = "/home/mint/Pictures/miao/prova/cricetone"
FORMAT = ["jpg", "pong"]
MAX_NESTING_DEPTH = "hour"
```
This will look for all the folder and subfolders inside `/home/mint/Pictures/` and look for files ending with `jpg` or `pong` (case insensitive) and then will copy the globbed files inside `/home/mint/Pictures/miao/prova/cricetone`. Folders are created dynamically when they do not exist.

New folders will be created when needed according to `MAX_NESTING_DEPTH`. In this example, pictures will be grouped in folders up to hours. Only needed folders will be created

```java
└── cricetone
    ├── anno-2016
    │   └── mese-12
    │       ├── giorno-26
    │       │   ├── ora-00
    │       │   ├── ora-02
    │       │   └── ora-03
    │       └── giorno-31
    │           ├── ora-04
    │           ├── ora-05
    │           ├── ora-06
    │           ├── ora-10
    │           ├── ora-11
    │           ├── ora-16
    │           └── ora-17
    └── anno-2017
        └── mese-08
            ├── giorno-28
            │   ├── ora-21
            │   └── ora-22
            ├── giorno-29
            │   ├── ora-09
            │   ├── ora-11
            │   ├── ora-13
            │   ├── ora-16
            │   └── ora-19
            ├── giorno-30
            │   ├── ora-08
            │   └── ora-11
            └── giorno-31
                ├── ora-09
                ├── ora-10
                ├── ora-11
                ├── ora-12
                └── ora-13

```
Files will be placed in the correct folder and renamed with the format `yyyy-MM-ddThh-mm-ss.XXX`, when `XXX` is its file extension. For security reasons, files are not moved or modified but copied instead.


Folder labels can be configured inside the `NESTED_FOLDERS` parameter by changing the values (`"anno-"` etc. ). **Do not edit or remove any dictionary key** (`"year"` etc.)
```python
NESTED_FOLDERS = {
    "year": "anno-",
    "month": "mese-",
    "day": "giorno-",
    "hour": "ora-",
    "minute": "minuto-",
}
```
The script is tested with Python 3.7.3. After setting your parameters you can run it with
```
python pictag.py
```


The script shows logs to identify the elaboration of each file, informing the user about ignored files and successful operations.


```java
[2019-04-20 22:47:00.545][INFO][<module>        ]  =============== Folder stats ===============
[2019-04-20 22:47:00.545][ LOG][<module>        ]  Walking inside folder      : /home/mint/Pictures/Barcellona 2017 GoPro
[2019-04-20 22:47:00.545][INFO][<module>        ]  Names of sibling folders   : ['second folder', 'first folder', 'rename']
[2019-04-20 22:47:00.545][INFO][<module>        ]  Files inside current folder: 20
[2019-04-20 22:47:00.554][WARN][createDirectory ]  /home/mint/Pictures/miao/prova/cricetone/anno-2017/mese-08/giorno-31/ora-12 does not exist. Creating directory
[2019-04-20 22:47:00.554][INFO][<module>        ]  Copying  file GOPR6164.JPG to 2017-08-31T12-57-05.JPG
[2019-04-20 22:47:00.559][WARN][createDirectory ]  /home/mint/Pictures/miao/prova/cricetone/anno-2017/mese-08/giorno-31/ora-13 does not exist. Creating directory
[2019-04-20 22:47:00.559][INFO][<module>        ]  Copying  file GOPR6167.JPG to 2017-08-31T13-02-43.JPG
[2019-04-20 22:47:00.563][INFO][<module>        ]  Copying  file GOPR6160.JPG to 2017-08-31T12-55-45.JPG
[2019-04-20 22:47:00.569][INFO][<module>        ]  Copying  file GOPR6168.JPG to 2017-08-31T13-02-47.JPG
[2019-04-20 22:47:00.571][WARN][<module>        ]  Ignoring file GOPR6174.MP4 ---> Reason: not globbed by ['jpg', 'pong'] filter
[2019-04-20 22:47:00.573][INFO][<module>        ]  Copying  file GOPR6163.JPG to 2017-08-31T12-56-52.JPG
[2019-04-20 22:47:00.577][INFO][<module>        ]  Copying  file GOPR6156.JPG to 2017-08-31T12-55-30.JPG
[2019-04-20 22:47:00.581][INFO][<module>        ]  Copying  file GOPR6158.JPG to 2017-08-31T12-55-12.JPG
[2019-04-20 22:47:00.585][INFO][<module>        ]  Copying  file GOPR6162.JPG to 2017-08-31T12-56-14.JPG
[2019-04-20 22:47:00.588][WARN][<module>        ]  Ignoring file GOPR6175.MP4 ---> Reason: not globbed by ['jpg', 'pong'] filter
[2019-04-20 22:47:00.590][INFO][<module>        ]  Copying  file GOPR6154.JPG to 2017-08-31T12-55-00.JPG
[2019-04-20 22:47:00.592][WARN][<module>        ]  Ignoring file GOPR6172.MP4 ---> Reason: not globbed by ['jpg', 'pong'] filter
[2019-04-20 22:47:00.592][WARN][<module>        ]  Ignoring file GOPR6169.MP4 ---> Reason: not globbed by ['jpg', 'pong'] filter
[2019-04-20 22:47:00.594][INFO][<module>        ]  Copying  file GOPR6159.JPG to 2017-08-31T12-55-16.JPG
[2019-04-20 22:47:00.598][INFO][<module>        ]  Copying  file GOPR6157.JPG to 2017-08-31T12-55-34.JPG
[2019-04-20 22:47:00.600][WARN][<module>        ]  Ignoring file GOPR6170.MP4 ---> Reason: not globbed by ['jpg', 'pong'] filter
[2019-04-20 22:47:00.601][INFO][<module>        ]  Copying  file GOPR6155.JPG to 2017-08-31T12-55-13.JPG
[2019-04-20 22:47:00.605][INFO][<module>        ]  Copying  file GOPR6165.JPG to 2017-08-31T13-02-30.JPG
[2019-04-20 22:47:00.612][INFO][<module>        ]  Copying  file GOPR6166.JPG to 2017-08-31T13-02-34.JPG
[2019-04-20 22:47:00.617][INFO][<module>        ]  Copying  file GOPR6161.JPG to 2017-08-31T12-56-09.JPG
[2019-04-20 22:47:00.620][ LOG][<module>        ]  WORK LOG on folder /home/mint/Pictures/Barcellona 2017 GoPro:
[2019-04-20 22:47:00.620][INFO][<module>        ]      Number of globbed files: 15
[2019-04-20 22:47:00.620][INFO][<module>        ]      Number of ignored files: 5
[2019-04-20 22:47:00.620][WARN][<module>        ]      List of ignored files  : ['GOPR6174.MP4', 'GOPR6175.MP4', 'GOPR6172.MP4', 'GOPR6169.MP4', 'GOPR6170.MP4']
```
