
from __future__ import print_function, division
import os
import numpy as np
from PIL import Image
from PIL.ExifTags import TAGS
from inspect import currentframe
from datetime import datetime
from colorama import Fore, Style
from glob import glob
import shutil


__version__ = "1.0"

"""
EXIF file tagger.

The script looks for Image files inside a certain directory and organized them
in folders according to their creation time

Author  : Piermarco Giobbi
Contact : piermarco.giobbi@gmail.com


Parameters
----------
PARENT_PATH : string
    Absolute path of the root folder of the incoming pictures. The script will
    walk through all its children folders. Example: "/home/usr/Pictures"

END_PATH : string
    Absolute path of the end folder. Nested folders will be created here

FORMAT : string, list(1d, strings)
    File extension to detect pictures inside `PARENT_PATH` and inside its
    children folders. Case insensitive. If a list of formats is provided, the
    script will glob all the given format strings.
    Example: "jpg"; or ["jpg", "joppg"]

PATH_FULL_FORMAT : boolean
    Verbosity of the output paths. If True, logs show the absolute path of all
    files and folders. If False, logs show only file names or folder names.
    Note: this only changes the log format and does not have any impact on the
    file processing

MAX_NESTING_DEPTH : string
    Nesting depth to group pictures according to their creation date.
    Allowed values: ["year", "month", "day", "hour", "minute"].
    Example: "year" will create a single folder for each year, "month" will
    create year folders and month folders inside them.

NESTED_FOLDERS: dictionary
    Mapping between date fields and corresponding folder names.
    Example: "year": "anno-" creates year folders with "anno-" before the
    corresponding year e.g. "anno-2018".

"""

PARENT_PATH = "/home/mint/Pictures/"
END_PATH = "/home/mint/Pictures/miao/prova/cricetone"
FORMAT = ["jpg", "pong"]
PATH_FULL_FORMAT = False
MAX_NESTING_DEPTH = "hour"
NESTED_FOLDERS = {
    "year": "anno-",
    "month": "mese-",
    "day": "giorno-",
    "hour": "ora-",
    "minute": "minuto-"
}

# ============================== CODE START ===================================


class Message:
    """Define a class to plot colored log messages."""
    def __init__(self):
        """Initialize the class."""
        self.log = "[" + Fore.GREEN + " LOG" + Style.RESET_ALL + "]"
        self.warn = "[" + Fore.YELLOW + "WARN" + Style.RESET_ALL + "]"
        self.err = "[" + Fore.RED + " ERR" + Style.RESET_ALL + "]"
        self.out = "[" + Fore.MAGENTA + " OUT" + Style.RESET_ALL + "]"
        self.info = "[INFO]"

    @classmethod
    def timeStr(cls):
        """Return the current UTC time up to milliseconds."""
        return "[" + datetime.utcnow().__str__()[:-3] + "]"

    @classmethod
    def callerStr(cls, caller):
        """Return what frame/function is printing the message."""
        return "[%-16s]" % caller.f_code.co_name

    def wholeMessage(self, method, caller, message):
        """Return the complete message."""
        return self.timeStr() + method + self.callerStr(caller) + "  %s" \
                                                                  % message


class Tree:
    # Tree class for directories
    def __init__(self, walk):
        self.dir, self.dirSibl, self.files = walk

    def __str__(self):
        return 'dirName = "%10s", Files = %10s' % (os.path.basename(self.dir),
                                                   self.files)

    def __repr__(self):
        return self.__str__()


def createDirectory(path):
    """Create a directory in the requested path."""
    if not os.path.exists(path):
        msg = "%s does not exist. Creating directory" % path
        print(m.wholeMessage(m.warn, currentframe(), msg))
        os.makedirs(path)


def get_exif(fn):
    """Get the exif object for a picture."""
    ret = {}
    try:
        i = Image.open(fn)
    except OSError as e:
        msg = "Ignoring file %s ---> Reason: %s" % (fn, e)
        print(m.wholeMessage(m.err, currentframe(), msg))
        return None
    try:
        info = i._getexif()
    except AttributeError as e:
        msg = "Ignoring file %s ---> Reason: %s" % (fn, e)
        print(m.wholeMessage(m.err, currentframe(), msg))
        return None

    for tag, value in info.items():
        decoded = TAGS.get(tag, tag)
        ret[decoded] = value
    return ret


def getYear(exif):
    return exif['DateTime'].split(":")[0]


def getMonth(exif):
    return exif['DateTime'].split(":")[1]


def getDay(exif):
    return exif['DateTime'].split(":")[2]


def getDate(exif):
    date, time = exif['DateTime'].split()
    yy, mm, dd = date.split(":")
    hh, mins, ss = time.split(":")
    return (yy, mm, dd), (hh, mins, ss)


def formatInFile(path):
    fileFormat = os.path.splitext(path)[1]
    for form in FORMAT:
        if form.lower() in fileFormat or form.upper() in fileFormat:
            return True
    else:  # if no break occurs
        return False


def findCopyPath(path):
    if os.path.exists(path):
        spl, format = os.path.splitext(path)
        newNumCopy = 1
        if spl[-1] != ")":
            spl += "(%i)" % newNumCopy
            path = findCopyPath(spl + format)
        else:
            # In case there is more than one '('
            *first, last = spl.split("(")
            first = '('.join(first)
            newNumCopy = int(last.split(')')[0]) + 1
            path = first + '(%i)' % newNumCopy + format
            path = findCopyPath(path)
    return path


def getFinalFolderPath(end_path, exif):
    (y, M, d), (h, m, s) = getDate(exif)
    y = NESTED_FOLDERS["year"] + y
    M = NESTED_FOLDERS["month"] + M
    d = NESTED_FOLDERS["day"] + d
    h = NESTED_FOLDERS["hour"] + h
    m = NESTED_FOLDERS["minute"] + m
    if MAX_NESTING_DEPTH == "year":
        return end_path + "/%s" % (y)
    if MAX_NESTING_DEPTH == "month":
        return end_path + "/%s/%s" % (y, M)
    if MAX_NESTING_DEPTH == "day":
        return end_path + "/%s/%s/%s" % (y, M, d)
    if MAX_NESTING_DEPTH == "hour":
        return end_path + "/%s/%s/%s/%s" % (y, M, d, h)
    if MAX_NESTING_DEPTH == "minute":
        return end_path + "/%s/%s/%s/%s/%s" % (y, M, d, h, m)
    else:
        raise ValueError("Something is not working :(")


global m
m = Message()

MAX_NESTING_DEPTH = MAX_NESTING_DEPTH.lower()
if MAX_NESTING_DEPTH not in ["year", "month", "day", "hour", "minute"]:
    raise ValueError("MAX_NESTING_DEPTH not understood. Choose between "
                     '["year", "month", "day", "hour", "minute"]')

# Print parameters
msg = "============= EXIF file rename ============="
print(m.wholeMessage(m.info, currentframe(), msg))

print(m.wholeMessage(m.info, currentframe(), ""))

msg = "================ Parameters ================"
print(m.wholeMessage(m.info, currentframe(), msg))

msg = "PARENT_PATH       : %s" % PARENT_PATH
print(m.wholeMessage(m.info, currentframe(), msg))

msg = "END_PATH          : %s" % END_PATH
print(m.wholeMessage(m.info, currentframe(), msg))

msg = "FORMAT            : %s" % FORMAT
print(m.wholeMessage(m.info, currentframe(), msg))

msg = "PATH_FULL_FORMAT  : %s" % PATH_FULL_FORMAT
print(m.wholeMessage(m.info, currentframe(), msg))

msg = "MAX_NESTING_DEPTH : %s" % MAX_NESTING_DEPTH
print(m.wholeMessage(m.info, currentframe(), msg))

msg = "============================================"
print(m.wholeMessage(m.info, currentframe(), msg))


PARENT_PATH = PARENT_PATH.rstrip("/")
END_PATH = END_PATH.rstrip("/")
FORMAT = np.atleast_1d(FORMAT).tolist()

foto = []
for i in range(len(FORMAT)):
    FORMAT[i] = FORMAT[i].lstrip(".")
    # Globbing both upper and lower cases
    uu = PARENT_PATH + '/*.' + FORMAT[i].upper()
    ll = PARENT_PATH + '/*.' + FORMAT[i].lower()
    fotoU = glob(uu)
    fotoL = glob(ll)
    foto = np.concatenate((foto, fotoU, fotoL))
foto = foto.tolist()

# Creating the requested final path
createDirectory(END_PATH)

rejectDirs = [END_PATH]
for elem in os.walk(PARENT_PATH):
    t = Tree(elem)

    print(m.wholeMessage(m.info, currentframe(), ""))
    msg = "=============== Folder stats ==============="
    print(m.wholeMessage(m.info, currentframe(), msg))

    msg = "Walking inside folder      : %s" % t.dir
    print(m.wholeMessage(m.log, currentframe(), msg))

    msg = "Names of sibling folders   : %s" % t.dirSibl
    print(m.wholeMessage(m.info, currentframe(), msg))

    msg = "Files inside current folder: %i" % len(t.files)
    print(m.wholeMessage(m.info, currentframe(), msg))

    if t.dir in rejectDirs:
        msg = "Ignoring folder ---> Reason: cannot walk inside destination " +\
              "folder"
        for direct in t.dirSibl:
            rejectDirs.append(t.dir + "/" + direct)
        print(m.wholeMessage(m.err, currentframe(), msg))
        continue

    globbed = []
    notGlobbed = []
    for file in t.files:
        file = t.dir + "/" + file

        # Print complete file path if requested
        fName = os.path.basename(file) if not PATH_FULL_FORMAT else file

        # Work only on the right format e.g. jpg
        if formatInFile(file):
            globbed.append(fName)
            exif = get_exif(file)
            if exif is not None:
                rightFinalPath = getFinalFolderPath(END_PATH, exif)
                if not os.path.exists(rightFinalPath):
                    createDirectory(rightFinalPath)

                yMd, hms = getDate(exif)
                fileName = "%s" % '-'.join(yMd) + "T%s" % '-'.join(hms) + \
                           "%s" % os.path.splitext(file)[1]

                finalPath = rightFinalPath + "/" + fileName

                # Find a unique end path for the files
                finalPath = findCopyPath(finalPath)
                fPath = os.path.basename(finalPath) if not PATH_FULL_FORMAT  \
                    else finalPath

                msg = "Copying  file %s to %s" % (fName, fPath)
                print(m.wholeMessage(m.info, currentframe(), msg))

                shutil.copy(file, finalPath)
            else:
                notGlobbed.append(fName)
        else:
            notGlobbed.append(fName)
            msg = "Ignoring file %s ---> Reason: not globbed by %s filter" \
                % (fName, FORMAT)
            print(m.wholeMessage(m.warn, currentframe(), msg))

    msg = "WORK LOG on folder %s:" % t.dir
    print(m.wholeMessage(m.log, currentframe(), msg))
    msg = "    Number of globbed files: %i" % len(globbed)
    print(m.wholeMessage(m.info, currentframe(), msg))
    msg = "    Number of ignored files: %i" % len(notGlobbed)
    print(m.wholeMessage(m.info, currentframe(), msg))

    if len(notGlobbed) >= 1:
        msg = "    List of ignored files  : %s" % repr(notGlobbed)
        print(m.wholeMessage(m.warn, currentframe(), msg))
